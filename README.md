### Welcome on my portfolio

# Consigne 
Nous vous demandons de réaliser et mettre en ligne votre portolio.

Il doit avoir au moins les éléments suivants :

    Présentation de votre parcours
    Présentation de vos compétences et des technologies que vous connaissez
    Présentation de vos projets (avec un lien vers un dépôt gitlab et, si possible, une version en ligne)
    Vos coordonnées ainsi qu'un formulaire de contact (qui fonctionne).
    En bonus, vous pouvez proposer une partie blog.

Votre portfolio doit être attractif et lisible.

# I Start with my kanban on trello

https://trello.com/b/7vjJzT5y/portfolio

# Installation 
 npm install parcel -bundler 
 npm init -y
 npx parcel index.html

 sudo npm install materialize-css@next

# Lien version en ligne 

http://amelle_eval.surge.sh/

 git clone git@gitlab.com:amelleouldselma/portolio.git

 cd 

 # Perfomance 92% pour mobile et 98% ordi
 https://developers.google.com/speed/pagespeed/insights/?url=http%3A%2F%2Famelle_portfolio.surge.sh%2F

 # Impacte écologique 41
 https://www.website-footprint.com/en/result?w=http:%2F%2Famelle_portfolio.surge.sh%2F

 # Eco-index B
 http://www.ecoindex.fr/resultats/?id=115184 

 # Rapiditer du site 
https://www.uptrends.fr/outils-gratuits/speed-test-site-web 

